import React, { Component } from 'react'
import { Router, Route, Link, cleanPath } from 'react-static'
import { SendContactMessage } from './utilities/ServerApi'
import {validateEmail} from './utilities/Util'
import { easeQuadOut } from 'd3-ease'
import { NodeGroup, Animate } from 'react-move'
import { withContext, getContext } from 'recompose'
import PropTypes from 'prop-types'
import { hot } from 'react-hot-loader'
import Button from './images/combined_sustain_bw.gif'
import ButtonSmall from './images/combined_load_bw.gif'
import Routes from 'react-static-routes'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import './app.css'

library.add(fab, fas)

class App extends Component {
  constructor(props) {
    super();
    this.state = {
      email: "",
      notification: "",
      showModal: false,
      successMessage: false,
      errorMessage: false
    }
  }

  showModal(val) {
    const state = {};
    state.showModal = val;
    this.setState(state);
  }

  showSuccessMessage(val) {
    const state = {};
    state.successMessage = val;
    this.setState(state);
  }

  showErrorMessage(val) {
    const state = {};
    state.errorMessage = val;
    this.setState(state);
  }

  OnSubmit(event) {
    event.preventDefault();
    if(!validateEmail(this.state.email)) {
      this.setState({notification: "You have used an improper email address"})
    } else {
      SendContactMessage(this.state.email).then(result=> {
        if(result.length == 0) {
          this.showErrorMessage(true)
        } else {
          this.showSuccessMessage(true)
        }
      })
    }
  }


  render() {
    return (
      <div>
        <div className="content content-main">

          <div className="main-button" 
               onClick={() => this.showModal(true) }>
            <img src={Button} />
          </div>

          {this.state.showModal && 
            <div>
              <div className="modal-wrapper" onClick={() => this.showModal(false)} />
              <Animate
                start={() => ({
                  opacity: [0],
                  scale: [0]
                })}
                enter={() => ({
                  opacity: [1],
                  scale: [1],
                  timing: { duration: 300, delay: 200, ease: easeQuadOut },
                })}
                update={() => ({
                  opacity: [1],
                })}
                leave={() => ({
                  opacity: [0],
                  scale: [0],
                  timing: { duration: 300, ease: easeQuadOut },
                })}
              >
              {({opacity, scale}) => {
                return (
                  <div className="modal-content" 
                        style={{
                          transform: ` translate(-50%, -50%) scale(${scale})`, 
                          opacity
                        }}
                  >
                    <img src={ButtonSmall} />
                    <h2>Subscribe for our future updates and events. <br /> We wouldn't bother you if it's not important. Promise!</h2>
                    <form 
                      onSubmit={(event) => this.OnSubmit(event)}
                    >
                      <div className="form-group">
                          <input
                              type="email"
                              name="email"
                              placeholder="your_email@gmail.com"
                              id="emailAddress"
                              onChange={(event) => this.setState({ email: event.target.value })}
                              required
                          />
                      </div>
                      <div className="row-submit"><input className="blue-button button submitButton" type="submit" value="Subscribe"/></div>
                    </form>
                  </div>
                )
              }}
              </Animate>
            </div>
          }

          {this.state.successMessage && 
            <div>
              <div className="modal-wrapper" style={{backgroundColor: 'rgba(171, 160, 181, 1)'}} onClick={() => {this.showSuccessMessage(false); this.showModal(false)}} />
              <div className="modal-content modal-content-error">
               Thank you for subscribing! Sending you LOVE!
              </div>
            </div>
          }

          {this.state.errorMessage && 
            <div>
              <div className="modal-wrapper" style={{backgroundColor: 'rgba(171, 160, 181, 1)'}} onClick={() => {this.showErrorMessage(false); this.showModal(false)}} />
              <div className="modal-content modal-content-error">
               Oh no...something went wrong. You can try again later or email us at <a className="footer-mail" href="mailto:info@albedoinformatics.com">info@albedoinformatics.com</a>
              </div>
            </div>
          }


          <div className="social-icons flex">
            <a href="https://www.facebook.com/thisislarge/" target="_blank"><FontAwesomeIcon icon={['fab', 'facebook']} /></a>
            <a href="https://www.instagram.com/this.is.large/" target="_blank"><FontAwesomeIcon icon={['fab', 'instagram']} /></a>
            <a href="https://twitter.com/large_app" target="_blank"><FontAwesomeIcon icon={['fab', 'twitter']} /></a>
          </div>

        </div>
      </div>
    )
  }
}


export default hot(module)(App)
