export async function SendContactMessage(email)
{
    var emailParam = "&contact_email="+email;

    var uri = "http://localhost:3000/"+emailParam;

    const response = await fetch(uri,{method:'POST',headers: {'Content-Type': 'application/json'}});

    const body = await response;

    if (response.status !== 200) throw Error(body.message);

    return body.text();
}